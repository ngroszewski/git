# git

Install and configure git

## Dependencies

* [polkhan.bash](https://gitlab.com/polkhan/asdf.git)
  _The bash configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

A description of the settable variables for this role should go here, including
any variables that are in defaults/main.yml, vars/main.yml, and any variables
that can/should be set via parameters to the role. Any variables that are read
from other roles and/or the global scope (ie. hostvars, group vars, etc.)
should be mentioned here as well.

```yaml
---
git:
  ignore:
    - .env
    - *.exe
  config:
    user:
      - name = Nick Groszewski
      - email = groszewn@example.com
    "color \"status\"":
      - added = green
      - changed = yellow
      - untracked = red
    submodule:
      - active = true
      - fetchJobs = 0
    status:
      - short = true
      - branch = true
    alias:
      - lf = log --first-parent
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.git

## License

MIT
